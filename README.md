# Team Cluster

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install
brew install redis
brew services start redis

```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev
```

## Note

The app needs:

- A personal access token to work, it stores it in the cookies, the necessary permission are: `read_api, read_user, read_repository` once obtained the PAT it needs to be added in `localhost:3000/manage`
- At least two people in the Team field in the same page
- A page refresh after the two points above are fulfilled
