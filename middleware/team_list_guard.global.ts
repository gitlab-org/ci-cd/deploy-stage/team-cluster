export default defineNuxtRouteMiddleware((to, from) => {
  const teamList = useCookie("teamList");
  if (to.path !== "/manage" && !teamList.value) {
    return navigateTo("/manage");
  }
});
