export default defineEventHandler(async (event) => {
    const { projectId, notableIds, noteable = 'issues' } = getQuery(event)

    return $fetch(`https://gitlab.com/api/v4/projects/${projectId}/${noteable}/`, { query: { 'iids[]': notableIds } })
})