import type { TeamMember, UserName } from "../../../universally_shared/types";

type QueryParams = {
    team: UserName[] | UserName
}

export default defineEventHandler(async (event) => {
    let { team = [] } = getQuery<QueryParams>(event)

    if (typeof team !== 'object') {
        team = [team]
    }

    const storedTeam = await useStorage('redis').getItem<UserName[]>('rawTeam')

    const isCached = storedTeam?.toString() === team.toString() && team?.length !== 0

    if (isCached) {
        return  await useStorage('redis').getItem<TeamMember[]>('team')
    }

    await useStorage('redis').setItem('rawTeam', team)

    const requests = team.map(userName => {
        return $fetch<TeamMember[]>('https://gitlab.com/api/v4/users?username=' + userName)
    });

    const response = (await Promise.all(requests)).flat()

    await useStorage('redis').setItem('team', response)

    return response;

})
