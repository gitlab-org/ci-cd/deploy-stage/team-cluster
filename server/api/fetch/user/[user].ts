

const globalHeaders = {
    'PRIVATE-TOKEN': null
}

type UserEvent = {
    action_name: string,
    target_type: string,
    id: number
}

type CategorizedContributions = {
    push: UserEvent[],
    comments: UserEvent[],
    openedMrs: UserEvent[],
    openedIssues: UserEvent[],
    closedMRs: UserEvent[],
    closedIssues: UserEvent[],
    approvedMRs: UserEvent[],
}

const groups = ['gitlab-com', 'gitlab-org']

const crawlPages = async (url: string, headers: HeadersInit = globalHeaders) => {
    const data = [];
    for (let page = 1; page < 50; page++) {
        const response = await $fetch(url, { headers, query: { per_page: 100, page } }) as Object[];
        data.push(...response);
        if (response.length < 100) {
            break;
        }
    }
    return data;
}

const getMrGroupsData = async (groups: string[], params: string) => {
    const calls = []

    for (const group of groups) {
        const url = `https://gitlab.com/api/v4/groups/${group}/merge_requests?${params}`;
        calls.push(crawlPages(url))
    }

    const promiseResult = await Promise.all(calls)

    return promiseResult.reduce((prev, current) => {
        return [...prev, ...current]
    }, [])
}

const generateCallParameters = (params: object, date_scope_binding = { before: 'created_before', after: 'created_after' }): string => {
    const currentDate = new Date()
    const lastWeekDate = new Date(currentDate.getTime() - 7 * 24 * 60 * 60 * 1000);
    const finalParams = {
        ...params,
        [date_scope_binding.before]: currentDate.toJSON(),
        [date_scope_binding.after]: lastWeekDate.toJSON()
    }
    return new URLSearchParams(finalParams).toString();
}

const getMRs = async (author_id: number | string) => {
    const params = generateCallParameters({ state: 'all', author_id })
    return getMrGroupsData(groups, params)

}

const getReviewedMRs = async (id: number | string) => {
    const params = generateCallParameters({ state: 'all', scope: 'all', 'approved_by_ids[]': id })
    return getMrGroupsData(groups, params)
}

const getContributions = async (id: number | string) => {
    const params = generateCallParameters({ scope: 'all' }, { before: 'before', after: 'after' })
    const contributions = await crawlPages(`http://www.gitlab.com/api/v4/users/${id}/events?${params}`, {}) as UserEvent[]
    const categorizedContributions: CategorizedContributions = {
        push: [],
        comments: [],
        openedMrs: [],
        openedIssues: [],
        closedMRs: [],
        closedIssues: [],
        approvedMRs: [],
    };

    for (const c of contributions) {
        if (c.action_name === "pushed to") {
            categorizedContributions.push.push(c);
        } else if (c.action_name === "commented on") {
            categorizedContributions.comments.push(c);
        } else if (c.action_name === "opened" && c.target_type === "MergeRequest") {
            categorizedContributions.openedMrs.push(c);
        } else if (c.action_name === "opened" && c.target_type === "Issue") {
            categorizedContributions.openedIssues.push(c);
        } else if (c.action_name === "closed" && c.target_type === "MergeRequest") {
            categorizedContributions.closedMRs.push(c);
        } else if (c.action_name === "closed" && c.target_type === "Issue") {
            categorizedContributions.closedIssues.push(c);
        } else if (
            c.action_name === "approved" &&
            c.target_type === "MergeRequest"
        ) {
            categorizedContributions.approvedMRs.push(c);
        }
    }
    return { contributions, categorizedContributions }

}

export default defineEventHandler(async (event) => {
    const accessToken = getCookie(event, "accessToken");

    globalHeaders['PRIVATE-TOKEN'] = accessToken
    const userId = getRouterParam(event, 'user') || ''

    const user = await $fetch(`http://www.gitlab.com/api/v4/users/${userId}`, { headers: globalHeaders })
    const { contributions, categorizedContributions } = await getContributions(userId)
    const mrs = await getMRs(userId)
    const reviewedMrs = await getReviewedMRs(userId)

    const response = { user, contributions, categorizedContributions, mrs, reviewedMrs };

    await useStorage('redis').setItem(userId, response)

    return response
})