export default defineEventHandler(async (event) => {
  const { ids = [] } = getQuery(event)

  const contributions = [];
  const targets = {};
  const heatMap = {};

  for (const id of ids) {
    const user = await useStorage('redis').getItem(`${id}`)
    if (user?.contributions) {
      contributions.push(...user.contributions)
    }
  }



  for (const c of contributions) {
    const t = c.target_id
    if (targets[t]) {
      targets[t].push(c);
    } else if (t) {
      targets[t] = [c]
    }
  }

  for (const t in targets) {
    const heat = targets[t].length
    if (heatMap[heat]) {
      heatMap[heat].push(t)
    } else if (t) {
      heatMap[heat] = [t]
    }
  }

  return { contributions, targets, heatMap };
})