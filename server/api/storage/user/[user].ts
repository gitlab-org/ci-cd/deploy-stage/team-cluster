export default defineEventHandler(async (event) => {
  const userId = getRouterParam(event, 'user') || ''
  return await useStorage('redis').getItem(`${userId}`)
})