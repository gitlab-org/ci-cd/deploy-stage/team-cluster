export type UrlString = string;
export type UserState = "active" | "blocked" | "deactivated";
export type UserName = string;

export type TeamMember = {
    id: number;
    username: UserName;
    name: string;
    state: UserState;
    locked: boolean;
    avatar_url: UrlString;
    web_url: UrlString;
};
